﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shepherd : MonoBehaviour
{

    float MaximumLife = 10;
    public static float MaxLifePerLevel = 2f;
    float CurrentLife;

    float DecayRatio = 1f;
    float DecayInterval = 5f;
    float LastDecay;

    public Image LifeBar;

    //Channeling vars
    bool IsChanneling = false;
    Sheep ChannelingSheep;

    bool IsDead = false;

    public float LifeDrainRatio;

    public AudioClip ChannelingSound;
    public AudioClip DeathSound;

    public GameObject BackgroundMusicObject;

    public GameObject GameOverPanel;
    public Text FinalScoreText;
    public Text LevelText;
    public static string SCORE_TEXT = "Score: {0}";
    public static float score = 0f;

    float startTime;

    public static int EvolutionStage = 0;
    public static int EvolutionStageExperience = 0;
    public static int[] EvolutionStageExperienceTable = { 1, 2, 4, 8 };

    public float LureSpeed = 2f;
    public static float LureSpeedPerLevel = 1f;

    public static int SheepCount = 0;

    void Start()
    {
        startTime = Time.time;

        BackgroundMusicObject.SetActive(true);

        LifeDrainRatio = 2f;

        LastDecay = Time.time;

        CurrentLife = 10f;
        MaximumLife = 10;

        LureSpeed = 2f;

        GetComponent<Movement>().Speed = 5f;
    }

    public void AddEvolution(int amount)
    {
        EvolutionStageExperience += amount;

        if(EvolutionStageExperience >= EvolutionStageExperienceTable[EvolutionStage])
        {
            if (EvolutionStage >= EvolutionStageExperienceTable.Length)
                return;

            EvolutionStageExperience -= EvolutionStageExperienceTable[EvolutionStage];
            EvolutionStage++;

            UpgradeStatus();
        }
    }

    void UpgradeStatus()
    {
        GetComponent<Movement>().Speed += 1f;

        MaximumLife += MaxLifePerLevel;

        LureSpeed += LureSpeedPerLevel;

        LevelText.text = (1 + EvolutionStage).ToString();
    }

    public void Replay()
    {
        EvolutionStage = 0;
        EvolutionStageExperience = 0;

        GameOverPanel.SetActive(false);

        Time.timeScale = 1f;

        GetComponent<AudioSource>().loop = true;

        UnityEngine.SceneManagement.SceneManager.LoadScene("PlayScene");
    }

    void Update()
    {
        UpdateDecay();
    }

    void UpdateDecay()
    {
        if (Time.time - LastDecay > DecayInterval)
        {
            LastDecay = Time.time;

            AddLife(-DecayRatio);
        }
    }

    public void DeathRoutine()
    {
        IsDead = true;

        GetComponent<InputHandler>().CanMove = false;
        GetComponent<Movement>().StopMoving();
        GetComponentInChildren<Animator>().SetTrigger("IsDying");

        GetComponent<AudioSource>().Stop();

        GetComponent<AudioSource>().loop = false;

        GetComponent<AudioSource>().clip = DeathSound;
        GetComponent<AudioSource>().Play();

        StartCoroutine(WaitDeathAndFreezeGame());
    }

    IEnumerator WaitDeathAndFreezeGame()
    {
        yield return new WaitForSeconds(3f);

        BackgroundMusicObject.SetActive(false);

        Time.timeScale = 0f;

        UpdateScoreDisplay();

        GameOverPanel.SetActive(true);
    }

    void UpdateScoreDisplay()
    {
        score = Time.time - startTime;
        score += EvolutionStage * 200f;
        FinalScoreText.text = string.Format(SCORE_TEXT, (int)Mathf.Round(score));
    }

    public void AddLife(float amount)
    {
        CurrentLife += amount;

        if (CurrentLife > MaximumLife)
            CurrentLife = MaximumLife;

        LifeBar.fillAmount = CurrentLife / MaximumLife;

        if (CurrentLife <= 0)
            DeathRoutine();
    }

    public void StartChanneling(Sheep sheep)
    {
        if (sheep.IsMating || IsDead)
            return;

        ChannelingSheep = sheep;

        GetComponent<InputHandler>().CanMove = false;
        GetComponent<Movement>().StopMoving();
        GetComponentInChildren<Animator>().SetBool("IsChanneling", true);

        GetComponent<AudioSource>().Stop();

        GetComponent<AudioSource>().clip = ChannelingSound;
        GetComponent<AudioSource>().Play();

        ChannelingSheep.StartFeedingShepherd(this);

        IsChanneling = true;
    }

    public void StopChanneling()
    {
        if (!IsChanneling)
            return;

        ChannelingSheep.IsFeedingShepherd = false;
        ChannelingSheep = null;

        GetComponent<InputHandler>().CanMove = true;
        GetComponentInChildren<Animator>().SetBool("IsChanneling", false);

        IsChanneling = false;

        GetComponent<AudioSource>().Stop();
    }

    public void MuteSound()
    {
        AudioListener.pause = !AudioListener.pause;
    }
}
