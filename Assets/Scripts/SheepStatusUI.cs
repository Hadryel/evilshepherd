﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SheepStatusUI : MonoBehaviour
{

    public static SheepStatusUI Instance;

    public GameObject Panel;
    public Image FoodFill;
    public Image WaterFill;
    public Image JoyFill;
    public Image HealthFill;

    public GameObject GenderMaleIcon;
    public GameObject GenderFemaleIcon;

    public Sheep CurrentSheep;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Update()
    {
        if (CurrentSheep != null)
        {
            Panel.SetActive(true);
            FoodFill.fillAmount = CurrentSheep.Status[Resource.Hunger] / CurrentSheep.MaximumStatus[Resource.Hunger];
            WaterFill.fillAmount = CurrentSheep.Status[Resource.Thirsty] / CurrentSheep.MaximumStatus[Resource.Thirsty];
            JoyFill.fillAmount = CurrentSheep.Status[Resource.Joy] / CurrentSheep.MaximumStatus[Resource.Joy];
            HealthFill.fillAmount = CurrentSheep.Status[Resource.Life] / CurrentSheep.MaximumStatus[Resource.Life];

            GenderMaleIcon.SetActive(CurrentSheep.GenderMale);
            GenderFemaleIcon.SetActive(!CurrentSheep.GenderMale);
        }
        else
        {
            Panel.SetActive(false);
        }
    }
}
