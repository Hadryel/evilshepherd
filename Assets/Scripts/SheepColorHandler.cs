﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepColorHandler : MonoBehaviour
{
    public Color[] MaleColor;
    public Color[] FemaleColor;

    public void ApplyEvolutionStage(int stage)
    {
        if (GetComponentInParent<Sheep>().GenderMale)
            GetComponent<SpriteRenderer>().color = MaleColor[stage];
        else
            GetComponent<SpriteRenderer>().color = FemaleColor[stage];
    }

}
