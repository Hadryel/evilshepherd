﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcePool : MonoBehaviour
{

    public Resource ResourceType;

    public float ResourcePerSecond;

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.transform.CompareTag("Sheep"))
        {
            var sheep = collider.GetComponent<Sheep>();
            sheep.AddResource(ResourceType, ResourcePerSecond * Time.deltaTime);
            sheep.GetComponentInChildren<Animator>().SetBool("IsEating", true);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.transform.CompareTag("Sheep"))
        {
            var sheep = collider.GetComponent<Sheep>();
            sheep.GetComponentInChildren<Animator>().SetBool("IsEating", false);
        }
    }
}

public enum Resource
{
    Life, Hunger, Joy, Thirsty
}
