﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    Movement Movement;

    [HideInInspector]
    public bool CanMove = true;

    void Start()
    {
        Movement = GetComponent<Movement>();
    }

    void Update()
    {
        GetDirectionInput();
    }

    void GetDirectionInput()
    {
        Vector2 direction = Vector2.zero;

        if (Input.GetKey(KeyCode.W))
        {
            direction.y = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            direction.y = -1;
        }

        if (Input.GetKey(KeyCode.D))
        {
            direction.x = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            direction.x = -1;
        }

        if (CanMove)
            Movement.MoveInDirection(direction);

        if (direction == Vector2.zero)
            Movement.StopMoving();
    }

}
