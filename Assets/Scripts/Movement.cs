﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Rigidbody2D Rb2d;
    private Animator Animator;

    [HideInInspector]
    public float Speed = 2f;

    public static AudioClip[] WalkingSound;
    AudioSource ObjectAudioSource;

    void Awake()
    {
        if (WalkingSound == null)
        {
            WalkingSound = new AudioClip[2];

            var audioList = Resources.LoadAll("Sounds", typeof(AudioClip));

            for (int i = 0; i < 2; i++)
            {
                WalkingSound[i] = audioList[i] as AudioClip;
            }
        }

        ObjectAudioSource = GetComponent<AudioSource>();
        ObjectAudioSource.clip = WalkingSound[0];

        Animator = GetComponentInChildren<Animator>();
        Rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {

    }

    public void MoveInDirection(Vector2 direction)
    {
        if (!Animator.GetBool("IsWalking"))
            PlayRandomWalkingSound();


        Animator.SetBool("IsWalking", true);

        if (direction.x != 0)
            UpdateFacingDirection(direction.x > 0);

        Rb2d.velocity = direction.normalized * Speed;
    }

    public void SurfToPosition(Vector2 position, float speed)
    {
        var direction = (position - (Vector2)transform.position).normalized;

        if (direction.x != 0)
            UpdateFacingDirection(direction.x > 0);

        Rb2d.velocity = direction.normalized * speed;
    }

    void PlayRandomWalkingSound()
    {
        ObjectAudioSource.clip = WalkingSound[Random.Range(0, WalkingSound.Length)];
        ObjectAudioSource.Play();
    }

    void StopPlayingWalkingSound()
    {
        ObjectAudioSource.Stop();
    }

    public void StopMoving()
    {
        if (Animator.GetBool("IsWalking"))
            StopPlayingWalkingSound();

        Animator.SetBool("IsWalking", false);
        Rb2d.velocity = Vector2.zero;
    }

    void UpdateFacingDirection(bool facingLeft)
    {
        if (facingLeft)
            transform.rotation = Quaternion.AngleAxis(180f, Vector3.up);
        else
            transform.rotation = Quaternion.identity;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        StopMoving();
    }
}
