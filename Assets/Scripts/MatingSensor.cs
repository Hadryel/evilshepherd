﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatingSensor : MonoBehaviour
{
    Sheep Self;

    private void Start()
    {

    }

    private void Awake()
    {
        Self = GetComponentInParent<Sheep>();
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (!enabled)
            return;

        Sheep candidate = collider.gameObject.GetComponent<Sheep>();

        if (candidate.IsMateCandidate(Self) && Self.IsMateCandidate(candidate))
        {
            candidate.StartMating(Self);
            Self.StartMating(candidate);

            enabled = false;
        }
    }

}
