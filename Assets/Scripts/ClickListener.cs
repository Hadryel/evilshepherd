﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickListener : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    Shepherd Shepherd;
    Sheep Self;

    private void Awake()
    {
        Shepherd = GameObject.Find("Player").GetComponent<Shepherd>();
        Self = GetComponentInParent<Sheep>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {

    }
    
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        SheepStatusUI.Instance.CurrentSheep = this.GetComponentInParent<Sheep>();
        Shepherd.StartChanneling(Self);
    }
    
    public void OnPointerUp(PointerEventData pointerEventData)
    {
        Shepherd.StopChanneling(); 
    }
}
