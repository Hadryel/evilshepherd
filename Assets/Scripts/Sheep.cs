﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : MonoBehaviour
{
    public Dictionary<Resource, float> Status;
    public Dictionary<Resource, float> MaximumStatus;

    public bool GenderMale;

    Movement Movement;

    IEnumerator CurrentAction;

    public bool IsMating = false;
    public bool IsFeedingShepherd = false;
    public bool IsDead = false;

    public float MatingInterval;
    public float MatingDuration;
    public float LastMatingFinish;
    public float MatingStartTime;

    //Child life cycle
    float SpawnTime;
    float MatureTime;
    static float MIN_MATURE_TIME = 25f;
    static float MAX_MATURE_TIME = 40f;
    public bool IsMature = false;


    static GameObject ChildPrefab;
    static GameObject LoveParticlesPrefab;
    static GameObject SheepRitualParticles;
    static GameObject ChildBornParticles;

    static Vector3 CHILD_SCALE = new Vector3(1f, 1f, 1f);
    static Vector3 MATURE_SCALE = new Vector3(2f, 2f, 2f);
    static Vector3 STAGE_SCALE = new Vector3(0.2f, 0.2f, 0.2f);

    public float HungerToLifeRatio = 2f;
    public float WaterToLifeRatio = 1f;
    public float HungerToWaterRatio = 4f;
    public float WaterToJoyRatio = 0.25f;

    float HungerDecayRatio = 0.2f;
    float ThirstyDecayRatio = 0.1f;
    float JoyDecayRatio = 0.1f;

    public AudioClip SlurpSound;
    public AudioClip SheepDeathSound;
    public AudioClip SheepNormalDeath;

    void Start()
    {
        Status = new Dictionary<Resource, float>();
        MaximumStatus = new Dictionary<Resource, float>();

        MaximumStatus.Add(Resource.Hunger, 15f);
        MaximumStatus.Add(Resource.Life, 15f);
        MaximumStatus.Add(Resource.Thirsty, 15f);
        MaximumStatus.Add(Resource.Joy, 10f);

        Status.Add(Resource.Hunger, 10f);
        Status.Add(Resource.Life, 15f);
        Status.Add(Resource.Thirsty, 10f);
        Status.Add(Resource.Joy, 5f);

        MatingInterval = 30f;
        LastMatingFinish = Time.time;
        MatingDuration = 10f;

        Movement = GetComponent<Movement>();

        if (ChildPrefab == null)
            ChildPrefab = Resources.Load("Sheep") as GameObject;

        if (LoveParticlesPrefab == null)
            LoveParticlesPrefab = Resources.Load("Particles/LoveParticles") as GameObject;

        if (ChildBornParticles == null)
            ChildBornParticles = Resources.Load("Particles/ChildBornParticles") as GameObject;

        if (SheepRitualParticles == null)
            SheepRitualParticles = Resources.Load("Particles/SheepRitualParticles") as GameObject;

        StartAction(Wander());

        Shepherd.SheepCount += 1;
    }

    public void Update()
    {
        if (IsFeedingShepherd || IsDead)
            return;

        UpdateLifeWithHungerAndThirsty();

        UpdateDecayRatio();

        CheckForDeath();

        if (GetComponentInChildren<Animator>().GetBool("IsEating") && GetComponent<Rigidbody2D>().velocity == Vector2.zero && GetComponent<AudioSource>().clip != SlurpSound)
        {
            GetComponent<AudioSource>().Stop();

            GetComponent<AudioSource>().clip = SlurpSound;
            GetComponent<AudioSource>().Play();
        }
    }

    void CheckForDeath()
    {
        if (Status[Resource.Life] <= 0)
        {
            BeginDeath(false);
        }
    }

    void BeginDeath(bool fromRitual)
    {
        if (IsDead)
            return;

        IsDead = true;

        GetComponentInChildren<Animator>().SetTrigger("IsDying");

        GetComponentInChildren<ClickListener>().gameObject.SetActive(false);
        GetComponentInChildren<FleeSensor>().gameObject.SetActive(false);


        if (!GenderMale && GetComponentInChildren<MatingSensor>() != null)
            GetComponentInChildren<MatingSensor>().gameObject.SetActive(false);

        this.enabled = false;

        if (fromRitual)
        {
            GameObject deathParticles = GameObject.Instantiate(SheepRitualParticles, transform);
            GetComponent<AudioSource>().Stop();

            GetComponent<AudioSource>().clip = SheepDeathSound;
            GetComponent<AudioSource>().Play();
        }
        else
        {
            GameObject deathParticles = GameObject.Instantiate(SheepRitualParticles, transform);
            GetComponent<AudioSource>().Stop();

            GetComponent<AudioSource>().clip = SheepNormalDeath;
            GetComponent<AudioSource>().Play();
        }

        Shepherd player = GameObject.Find("Player").GetComponent<Shepherd>();

        player.AddEvolution(1);

        Shepherd.SheepCount -= 1;

        if(Shepherd.SheepCount == 0)
        {
            player.DeathRoutine();
        }

        StartCoroutine(WaitDeathAnimation());
    }

    IEnumerator WaitDeathAnimation()
    {
        yield return new WaitForSeconds(2f);

        Destroy(gameObject);
    }

    void UpdateDecayRatio()
    {
        Status[Resource.Hunger] -= Time.deltaTime * HungerDecayRatio;
        Status[Resource.Thirsty] -= Time.deltaTime * ThirstyDecayRatio;
        Status[Resource.Joy] -= Time.deltaTime * JoyDecayRatio;
    }

    void UpdateLifeWithHungerAndThirsty()
    {
        if (Status[Resource.Life] < MaximumStatus[Resource.Life])
        {
            float amount = 0f;
            if (Status[Resource.Hunger] > 0)
            {
                //This is not the right amount but will do
                Status[Resource.Hunger] -= HungerToLifeRatio * Time.deltaTime;
                amount += HungerToLifeRatio * Time.deltaTime;
            }
            else
            {
                Status[Resource.Life] -= HungerToLifeRatio * Time.deltaTime;
            }

            if (Status[Resource.Thirsty] > 0)
            {
                //This is not the right amount but will do
                Status[Resource.Thirsty] -= WaterToLifeRatio * Time.deltaTime;
                amount += WaterToLifeRatio * Time.deltaTime;
            }
            else
            {
                Status[Resource.Hunger] -= WaterToLifeRatio * Time.deltaTime;
            }

            if (amount > 0)
                AddResource(Resource.Life, amount);
        }
        else if (Status[Resource.Hunger] <= 0)
        {
            if (Status[Resource.Thirsty] > 0)
            {
                //This is not the right amount but will do
                Status[Resource.Thirsty] -= WaterToLifeRatio * Time.deltaTime;
                AddResource(Resource.Hunger, WaterToLifeRatio * Time.deltaTime);
            }
            else
            {
                Status[Resource.Hunger] -= HungerToLifeRatio * Time.deltaTime;
            }
        }

    }

    public IEnumerator Wander()
    {
        Vector2 randomDirection = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
        float duration = Random.Range(1f, 3f);

        Movement.MoveInDirection(randomDirection);

        float startTime = Time.time;

        while (Time.time - startTime < duration)
            yield return null;

        Movement.StopMoving();

        yield return new WaitForSeconds(Random.Range(0f, 5f));

        StartAction(Wander());
    }

    public IEnumerator MateRitual(Sheep mate)
    {
        GameObject loveParticles = GameObject.Instantiate(LoveParticlesPrefab, transform);

        GetComponentInChildren<Animator>().SetBool("IsLoving", true);

        while (Time.time - MatingStartTime < MatingDuration)
        {
            Movement.MoveInDirection(mate.transform.position - transform.position);
            yield return null;
        }

        GetComponentInChildren<Animator>().SetBool("IsLoving", false);

        IsMating = false;

        SpawnChild();

        StartCoroutine(MateRitualCooldown());
        StartAction(Wander());
    }

    void SpawnChild()
    {
        //Check to ensure single spawn
        if (!GenderMale)
        {
            GameObject child = GameObject.Instantiate(ChildPrefab);

            child.transform.localScale = CHILD_SCALE;

            child.GetComponent<Sheep>().GenderMale = (Random.Range(0, 2) > 0) ? true : false;

            child.GetComponent<Sheep>().StartMatureWaiting(Random.Range(MIN_MATURE_TIME, MAX_MATURE_TIME));

            child.GetComponentsInChildren<SheepColorHandler>()[0].ApplyEvolutionStage(Shepherd.EvolutionStage);
            child.GetComponentsInChildren<SheepColorHandler>()[1].ApplyEvolutionStage(Shepherd.EvolutionStage);

            GameObject loveParticles = GameObject.Instantiate(LoveParticlesPrefab, child.transform);
        }

    }

    public IEnumerator MateRitualCooldown()
    {
        yield return new WaitForSeconds(MatingInterval);

        if (!GenderMale && GetComponentInChildren<MatingSensor>() != null)
        {
            GetComponentInChildren<MatingSensor>().enabled = true;
            GetComponentInChildren<MatingSensor>().gameObject.SetActive(true);
        }

    }

    public void StartMatureWaiting(float matureTime)
    {
        StartCoroutine(WaitTillMature(matureTime));
    }

    public IEnumerator WaitTillMature(float matureTime)
    {
        yield return new WaitForSeconds(matureTime);

        Mature();
    }

    public void Mature()
    {
        IsMature = true;

        transform.localScale = MATURE_SCALE + (STAGE_SCALE * Shepherd.EvolutionStage);

        StartCoroutine(MateRitualCooldown());
    }

    public IEnumerator FeedingShepherd(Shepherd shepherd)
    {
        Movement.StopMoving();

        GetComponentInChildren<Animator>().SetBool("IsTakingDamage", true);

        IsFeedingShepherd = true;

        while (IsFeedingShepherd && Status[Resource.Life] > 0)
        {
            yield return null;

            Movement.SurfToPosition(shepherd.transform.position, shepherd.LureSpeed);

            shepherd.AddLife(Time.deltaTime * shepherd.LifeDrainRatio);
            Status[Resource.Life] -= Time.deltaTime * shepherd.LifeDrainRatio;
            Status[Resource.Joy] -= Time.deltaTime * shepherd.LifeDrainRatio;

            if (Status[Resource.Life] <= 0)
            {
                IsFeedingShepherd = false;

                BeginDeath(true);

                shepherd.StopChanneling();
            }
        }

        GetComponentInChildren<Animator>().SetBool("IsTakingDamage", false);

        StartAction(Wander());
    }

    public void StartFeedingShepherd(Shepherd shepherd)
    {
        StartAction(FeedingShepherd(shepherd));
    }

    public void StartMating(Sheep partner)
    {
        IsMating = true;

        Movement.StopMoving();

        MatingStartTime = Time.time;

        StartAction(MateRitual(partner));
    }

    public void AddResource(Resource type, float amount)
    {
        if (type == Resource.Joy)
        {
            if (Status[Resource.Joy] < MaximumStatus[Resource.Joy])
            {
                Status[Resource.Thirsty] -= amount * WaterToJoyRatio;
            }
        }
        
        if (Status[type] < MaximumStatus[type])
            Status[type] += amount;
    }

    void StartAction(IEnumerator action)
    {
        StopCurrentAction();
        CurrentAction = action;
        StartCoroutine(action);
    }

    void StopCurrentAction()
    {
        if (CurrentAction != null)
            StopCoroutine(CurrentAction);
    }

    public bool IsMateCandidate(Sheep candidate)
    {
        if (Status[Resource.Joy] < MaximumStatus[Resource.Joy] / 2)
            return false;

        //Game jam bugs
        if (candidate.IsFeedingShepherd)
            return false;

        if (candidate.GenderMale == this.GenderMale)
            return false;

        if (!candidate.IsMature)
            return false;

        if (IsMating)
            return false;

        return true;
    }
}
