﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeSensor : MonoBehaviour {

    Movement Movement;

	void Start () {
        Movement = GetComponentInParent<Movement>();
	}

    private void OnTriggerStay2D(Collider2D collider)
    {
        var fleeDirection = transform.position - collider.transform.position;
        Movement.MoveInDirection(fleeDirection);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Movement.StopMoving();
    }
}
